package com.aswdc.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName;
    ImageView ivClose;
    TextView tvDisplay;
    Button btnSubmit;
    ImageView ivBackground;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        intViewEvent();
        setTypefaceOnView();
        handleSavedInstance(savedInstanceState);
    }

    void handleSavedInstance(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            Log.d("BUNDLE VALUE:::", "" + savedInstanceState.toString());
            tvDisplay.setText(savedInstanceState.getString("TextViewValue"));
        }
    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
    }

    void intViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                HashMap<String, Object> map = new HashMap<>();
//                map.put("UserId", 1);
//                map.put("UserName", "Utsav");
//                map.put("Password", "Learning");
//                map.put("FirstName", "Sonagra");
//                map.put("LastName", "Sonagra");
//                map.put("DOB", "30/04/2001");

//                Log.d(":1:", "" + map.get("UserName"));
//                Log.d(":2:", "" + map.get("UserId"));


//                ArrayList<String> un = new ArrayList<>();
//                String name = "Utsav";
//                un.add(name);
//                name = "Parth";
//                un.add(name);
//                name = "Bansi";
//                un.add(name);
//                name = "Manali";
//                un.add(name);
//                name = "Shyam";
//                un.add(name);
//
//                ArrayList<String> fn = new ArrayList<>();
//                fn.add("Sonagra");
//                fn.add("Patel");
//                fn.add("Mamtora");
//                fn.add("Sangani");
//                fn.add("Mendapara");
//
//                ArrayList<String> ln = new ArrayList<>();
//                ln.add("Utsav");
//                ln.add("Parth");
//                ln.add("Bansi");
//                ln.add("Manali");
//                ln.add("Shyam");
//
//                ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
//                HashMap<String, Object> map = new HashMap<>();
//                map.put("UserName", "Utsav");
//                map.put("FirstName", "Sonagra");
//                map.put("LastName", "Utsav");
//                userList.add(map);
//
//                map = new HashMap<>();
//                map.put("UserName", "Parth");
//                map.put("FirstName", "Patel");
//                map.put("LastName", "Parth");
//                userList.add(map);
//
//                for (int i = 0; i < fn.size(); i++) {
//                    Log.d("FN::", "" + fn.get(i));
//                }
//
//                for (int i = 0; i < ln.size(); i++) {
//                    Log.d("LN::", "" + ln.get(i));
//                }
//
//                for (int i = 0; i < un.size(); i++) {
//                    Log.d("UN::", "" + un.get(i));
//                }
//
//                for (int i = 0; i < userList.size(); i++) {
//                    Log.d("userList::", "" + userList.get(i).get("UserName") + ":" + userList.get(i).get("FirstName"));
//                }




                /* EXPLISIT INTENT */
//                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
//                String contactTempString = etFirstName.getText().toString() + " " + etLastName.getText().toString();
//                int intValue = Integer.parseInt(etFirstName.getText().toString());
//                intent.putExtra("finalValue", contactTempString);
//                intent.putExtra("IntValue", intValue);
//                intent.putExtra("FinalAnswer", 45698);
//                startActivity(intent);

                Toast.makeText(getApplicationContext(), rbMale.isChecked() ? "Male" : "Female", Toast.LENGTH_LONG).show();
            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* IMPLISIT INTENT */
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
                startActivity(intent);
            }
        });
        tvDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                }
            }
        });
    }

    void initViewReference() {
        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActPhoneNumber);
        ivClose = findViewById(R.id.ivActClose);
        tvDisplay = findViewById(R.id.tvActDisplay);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivBackground = findViewById(R.id.ivActBackground);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbFootBall = findViewById(R.id.chbActFootBall);
        chbCricket = findViewById(R.id.chbActCricket);
        chbHockey = findViewById(R.id.chbActHockey);

    }
}